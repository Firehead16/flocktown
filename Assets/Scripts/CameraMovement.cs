﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float cameraSpeed = 2f;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //PC DEBUG
        transform.Translate(new Vector3(Input.GetAxis("Horizontal")/2f, 0, Input.GetAxis("Vertical"))/2f, Space.World);
        //
        if (Input.touchCount == 1)
        {
            Touch currentTouch = Input.GetTouch(0);
            if (currentTouch.phase == TouchPhase.Moved)
            {
                transform.Translate(new Vector3(-currentTouch.deltaPosition.x * cameraSpeed * Time.deltaTime, 0, -currentTouch.deltaPosition.y*cameraSpeed*Time.deltaTime), Space.World);
            }
        }
    }
}
